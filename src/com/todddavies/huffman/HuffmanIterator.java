
import java.util.Iterator;
import java.util.Stack;


/**
 * An iterator to iterate over the leaves in a HuffmanTree
 * Uses a successor algorithm to find the next leaves
 */
public class HuffmanIterator<T extends Comparable<T>> implements Iterator<T> {
  private Stack<HuffmanTree<T>> stack;
  private final boolean descending;
  
  public HuffmanIterator(HuffmanTree<T> tree, boolean descending) {
    this.stack = new Stack<HuffmanTree<T>>();
    this.descending = descending;
    addBranch(tree);
  }
  
  public HuffmanIterator(HuffmanTree<T> tree) {
    this(tree, true);
  }
  
  /**
   * Add a whole (side of a) branch to the stack
   * @param tree
   */
  private void addBranch(HuffmanTree<T> tree) {
    while(tree != null) {
      stack.push(tree);
      tree = descending ? tree.right : tree.left;
    }
  }

  @Override
  public boolean hasNext() {
    return !stack.isEmpty();
  }

  @Override
  public T next() {
    HuffmanTree<T> nextLeaf = nextLeaf();
    if(nextLeaf != null) return nextLeaf.value;
    else return null;
  }
  
  /**
   * @return The next HuffmanTree<T> leaf in the iteration order
   */
  public HuffmanTree<T> nextLeaf() {
    HuffmanTree<T> top = stack.pop();
    if(top != null) {
      if(top.leaf && !stack.isEmpty()) {
        HuffmanTree<T> parent = stack.pop();
        if(parent != null) {
          if((descending && parent.left != null) || (!descending && parent.right != null)) {
            addBranch(descending ? parent.left : parent.right);
          }
        }
        stack.push(parent);
      }
    }
    if(top == null || top.leaf) {
      return top;
    } else {
      return nextLeaf();
    }
  }

  @Override
  public void remove() {
    throw new UnsupportedOperationException("HuffmanTrees are immutable!");
  }
}
