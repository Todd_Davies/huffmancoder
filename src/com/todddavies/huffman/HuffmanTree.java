
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.SortedSet;
import java.util.Stack;

import org.apache.commons.math3.fraction.Fraction;

/**
 * A HuffmanEncoding tree class
 * Provides functions for generating a tree from a list of elements and
 * their probabilities, encoding and decoding elements.
 *
 * @param <T> The type of the data we're encoding and decoding
 */
public class HuffmanTree<T extends Comparable<T>> extends Ranked<T> implements Iterable<T>, Comparable<Ranked<T>>{
  // The left and right children of the tree
  public final HuffmanTree<T> left, right;
  // The parent (the tree is built from the bottom, so the parent isn't final)
  // TODO: Use a HuffmanTreeFactory to make the parent final
  private HuffmanTree<T> parent;
  // Is the tree a leaf?
  public final boolean leaf;
  
  private HuffmanTree(Ranked<T> ranked) {
    this(ranked.value, ranked.probability);
  }
  
  private HuffmanTree(T value, Fraction probability) {
    super(value, probability);
    left = null;
    right = null;
    leaf = true;
    parent = null;
  }
  
  /**
   * Creates a new tree constructed of the left and right trees
   * @param left The left branch
   * @param right The right branch
   */
  private HuffmanTree(Ranked<T> left, Ranked<T> right) {
    super(null, left.probability.add(right.probability));
    // If the left is a leaf, turn it into a HuffmanTree
    if(!(left instanceof HuffmanTree<?>)) left = new HuffmanTree<T>(left);
    this.left = ((HuffmanTree<T>) left).setParent(this);
    // If the right is a leaf, turn it into a HuffmanTree
    if(!(right instanceof HuffmanTree<?>)) right = new HuffmanTree<T>(right);
    this.right = ((HuffmanTree<T>) right).setParent(this);
    leaf = false;
  }
  
  /**
   * Creates a new HuffmanTree from a list of input elements
   * @param input A list of Ranked T's (where each element has an assigned probability)
   * @return A HuffmanTree configured for the input data
   */
  public static <T extends Comparable<T>> HuffmanTree<T> createTree(SortedSet<Ranked<T>> input) {
    if(input == null || input.size() == 0) return null;
    while(true) {
      Ranked<T> bottom = input.first();
      input.remove(bottom);
      Ranked<T> nextBottom = input.first();
      input.remove(nextBottom);
      HuffmanTree<T> newBranch = new HuffmanTree<T>(bottom, nextBottom);
      if(input.isEmpty()) return newBranch;
      else input.add(newBranch);
    }
  }
  
  public HuffmanTree<T> findLeaf(T input) {
    HuffmanIterator<T> it = new HuffmanIterator<T>(this);
    HuffmanTree<T> child = null;
    while(it.hasNext()) {
      child = it.nextLeaf();
      if(input.equals(child.value)) return child;
    }
    return null;
  }
  
  /**
   * Encode a List of T's
   * @param in The list to encode
   * @param out The BinaryOut to write to
   * @return A boolean indicating the success of the write
   */
  public boolean encode(List<T> in, BinaryOut out) {
    for(T item : in) {
      if(!encodeSingle(item, out)) {
        return false;
      }
    }
    return true;
  }
  
  /**
   * Encode a single T
   * @param item The item to encode
   * @param out The BinaryOut to write to
   * @return A boolean indicating the success of the write
   */
  public boolean encodeSingle(T item, BinaryOut out) {
    HuffmanTree<T> leaf = findLeaf(item);
    if(leaf == null) return false;
    // Write the leading 1
    out.write(true);
    // Create a stack to write the bytes in reverse order
    Stack<Boolean> stack = new Stack<Boolean>();
    // While we're not at the top of the tree
    while(leaf.parent != null) {
      // Keep track of this leaf
      HuffmanTree<T> child = leaf;
      // Get the parent
      leaf = leaf.parent;
      // Write 1/0 for the left/right child
      if(leaf.left == child) stack.push(true);
      else if(leaf.right == child) stack.push(false);
      // We've got an invalid tree!
      else throw new RuntimeException("Invalid tree"); 
    }
    // Write the bytes in the stack
    while(!stack.isEmpty()) out.write(stack.pop());
    return true;
  }
  
  /**
   * Decodes a List of T's
   * @param in The BinaryIn to read from
   * @return the list of T's to decode
   */
  public List<T> decode(BinaryIn in) {
    ArrayList<T> out = new ArrayList<T>();
    while(!in.isEmpty()) {
      out.add(decodeSingle(in));
    }
    return out;
  }
  
  /**
   * Decodes a single T
   * @param in The BinaryIn to read from
   * @return The decoded T
   */
  private T decodeSingle(BinaryIn in) {
    HuffmanTree<T> tree = null;
    // Check that it starts with a 1
    if(in.readBoolean()) {
      // Keep track of the tree we're on
      tree = this;
      // While we've not run out of tree or we've got a leaf
      while(tree != null && !tree.leaf) {
        // Read in the next bit
        boolean bit = in.readBoolean();
        // Branch in the required direction
        tree = bit ? tree.left : tree.right;
      }
    }
    // Return the value of the tree
    return tree == null ? null : tree.value;
    
  }
  
  
  /**
   * Set the parent of this tree
   * @param parent The new parent of this tree 
   * @return This tree object (for convenience)
   */
  private HuffmanTree<T> setParent(HuffmanTree<T> parent) {
    this.parent = parent;
    return this;
  }

  public StringBuilder toString(StringBuilder prefix, boolean isTail, StringBuilder sb) {
    if(right!=null) {
        right.toString(new StringBuilder().append(prefix).append(isTail ? "│   " : "    "), false, sb);
    }
    String nodeName = value == null ? "" : value.toString();
    sb.append(prefix).append(isTail ? "└── " : "┌── ").append(nodeName).append("\n");
    if(left!=null) {
        left.toString(new StringBuilder().append(prefix).append(isTail ? "    " : "│   "), true, sb);
    }
    return sb;
  }

  @Override
  public String toString() {
      return this.toString(new StringBuilder(), true, new StringBuilder()).toString();
  }
  
  public List<T> getElements() {
    Iterator<T> it = iterator(false);
    List<T> list = new LinkedList<T>();
    while(it.hasNext()) {
      list.add(it.next());
    }
    return list;
  }
  
  @Override
  public Iterator<T> iterator() {
    return new HuffmanIterator<T>(this);
  }
  
  public HuffmanIterator<T> iterator(boolean descending) {
    return new HuffmanIterator<T>(this, descending);
  }
  
  @Override
  public int compareTo(Ranked<T> other) {
    return probability.compareTo(other.probability);
  }
}
