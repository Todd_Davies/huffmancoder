
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.math3.fraction.Fraction;
import org.junit.Before;
import org.junit.Test;


public class HuffmanTreeTest {
  
  //What the codes *should* be
  byte[][] testBytes = { {(byte)0b1110},  // 1 - 1110
                         {(byte)0b1100},  // 2 - 1100
                         {(byte)0b1101},  // 3 - 1101
                         {(byte)0b10},    // 4 - 10
                         {(byte)0b1111} };// 5 - 1111
  // The five strings to encode
  String[] strings = {"1","2","3","4","5"};
  // The probabilities to use when building the tree
  Fraction[] probabilities = { new Fraction(0.1),
                               new Fraction(0.13), 
                               new Fraction(0.12), 
                               new Fraction(0.6), 
                               new Fraction(0.05)};
  static HuffmanTree<String> tree;
  
  @Before
  public void setup() {
    if(tree == null) {
      // Create a set of Ranked Strings for the tree generator
      SortedSet<Ranked<String>> input = new TreeSet<Ranked<String>>();
      for(int i = 0; i < strings.length; i++) {
        input.add(new Ranked<String>(strings[i], probabilities[i]));
      }
      // Create the tree
      tree = HuffmanTree.createTree(input);
      System.out.println(tree);
    }
  }
  
  
  /**
   * Assert that the elements were inserted in the right order and that
   * they all got inserted
   */
  @Test
  public void testCreateTree() {
    String word = "";
    for(String s : tree.getElements()) { word += s; }
    assertTrue(word.equals("51324"));
  }
  
  @Test
  public void testInspectOutput() {
    // Encode and write
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    BinaryOut output = new BinaryOut(out);
    tree.encode(Arrays.asList(strings), output);
    output.flush();
    byte[] bytes = out.toByteArray();
    BinaryIn in = new BinaryIn(new ByteArrayInputStream(bytes));
    while(!in.isEmpty()) System.out.print(in.readBoolean() ? "1" : "0");
    System.out.println();
  }
  
  /**
   * Encode all the strings and see if they're the same as the hardcoded
   * ones I worked out
   */
  @Test
  public void testEncodeDecode() {
    List<String> data = new LinkedList<String>();
    Random rand = new Random();
    for(int i = 0; i < 50; i++) data.add(strings[rand.nextInt(strings.length)]);  
    // Encode and write
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    BinaryOut output = new BinaryOut(out);
    tree.encode(data, output);
    output.flush();
    byte[] bytes = out.toByteArray();
    // Read and decode
    ByteArrayInputStream in = new ByteArrayInputStream(bytes);
    BinaryIn input = new BinaryIn(in);
    List<String> read = tree.decode(input);
    // Assert they're the same
    for(int i = 0; i < data.size(); i++) {
      assertEquals(data.get(i), read.get(i));
    }
  }
}
