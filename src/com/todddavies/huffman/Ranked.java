
import org.apache.commons.math3.fraction.Fraction;


/**
 * A wrapper around a type T that lets you assign a probability
 * to an instance of T 
 * @param <T>
 */
public class Ranked<T> implements Comparable<Ranked<T>> {
  public final T value;
  public final Fraction probability;
  
  /**
   * @param value The object
   * @param probability The probability of this instance
   */
  public Ranked(T value, Fraction probability) {
    this.value = value;
    this.probability = probability;
  }

  @Override
  public int compareTo(Ranked<T> other) {
    return probability.compareTo(other.probability);
  }

  @Override
  public String toString() {
    String valueString = null;
    if(value != null) {
      valueString = value.toString();
    }
    return "[" + valueString + ", " + probability.toString() + "]";
  }
  
}
